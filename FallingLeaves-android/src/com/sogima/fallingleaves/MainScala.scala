package com.sogima.fallingleaves

import com.badlogic.gdx.backends.android.AndroidApplication
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration

import android.os.Bundle


class MainScala extends AndroidApplication{
  override def onCreate(savedInstanceState: Bundle){
    super.onCreate(savedInstanceState)
    val cfg = new AndroidApplicationConfiguration()
    cfg.useGL20 = false  
    initialize(new GameMaster(), cfg)
  }
  
}
