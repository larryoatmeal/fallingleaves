package com.sogima.fallingleaves

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration

object MainScala {
  def main(args: Array[String]): Unit = {
	val cfg = new LwjglApplicationConfiguration()
	cfg.title = "scalagame"
	cfg.useGL20 = false
	cfg.width = 480
	cfg.height = 320
	new LwjglApplication(new GameMaster(), cfg)
  }
}