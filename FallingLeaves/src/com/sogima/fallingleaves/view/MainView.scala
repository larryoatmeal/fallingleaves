package com.sogima.fallingleaves.view

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Logger

import com.sogima.fallingleaves.model._


object MainView{
	//*Symbolic Units
	val WIDTH = MainModel.WIDTH
	val HEIGHT = MainModel.HEIGHT
}


class MainView(model: MainModel) {
	//*Setup components
	import MainView._
  
	Texture.setEnforcePotImages(false)
	
	val batch = new SpriteBatch(5460)
	val cam = new OrthographicCamera(WIDTH, HEIGHT)

	//*Load assets
	val background = new Texture(Gdx.files.internal("images/Background.png"))
	val personRight = new Texture(Gdx.files.internal("images/Person.png"))
	val personLeft = new Texture(Gdx.files.internal("images/PersonLeft.png"))
	val leaf = new Texture(Gdx.files.internal("images/Leaf.png"))
	
  
	def render(){//Don't need time component, taken care of in main loop
		//*Setup camera (x, y, z)
		cam.position.set(WIDTH/2, HEIGHT/2, 0)
		//Sets middle of the screen to given coordinates
		//(0,0) is bottom left corner
		
		cam.update()
		batch.setProjectionMatrix(cam.combined)

		//*Render loop
		batch.begin()
			// Bottom left corner at x, y
			
		
			batch.draw(background, 0, 0, 30, 20)
			
			
			if(model.person.facingLeft){
				batch.draw(personLeft, model.person.x, model.person.y, Person.Width, Person.Height)
			}else{
				batch.draw(personRight, model.person.x, model.person.y, Person.Width, Person.Width)
			}
		
			model.leaves.foreach{
				mleaf => {
					batch.draw(leaf, mleaf.x, mleaf.y, Leaf.Width, Leaf.Height)
				}
			}
		
		
			
		
		batch.end()

	}
}