package com.sogima.fallingleaves.model

import scala.collection.mutable.Queue
import scala.collection.mutable.ArrayBuffer

object MainModel{
	//WORLD UNITS
	val WIDTH = 30
	val HEIGHT = 20
  
  
}


class MainModel {
	//Model classes that make up entire world 
	import MainModel._
	val person = new Person(WIDTH/2, 0)
	
	var leaves = new ArrayBuffer[Leaf]()
	
	//Update each entity
	def update(delta: Float){
	  
	  //Leaves	  
	  Leaf.timebomb += delta
	  if(Leaf.timebomb > Leaf.spawnTime){
	    leaves += Leaf.newLeaf()
	    Leaf.timebomb = 0
//	    print(leaves.length)
	  }
	  leaves = leaves.filter{//PERFORMANCE?
	    leaf => {
	      !(leaf.hitsGround || CollisionUtility.RectangleOverlap(person.rectangle, leaf.rectangle))
	    }
	  }
	  leaves.foreach{
	    leaf => leaf.update(delta)
	  }
	  
	  
	  
	  
	  
	  
	  
	  person.update(delta)
	  
	}
}