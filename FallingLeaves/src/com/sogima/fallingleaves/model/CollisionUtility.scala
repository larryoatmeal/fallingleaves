package com.sogima.fallingleaves.model


case class Rectangle(x: Float, y: Float, w: Float, h: Float)


object CollisionUtility {
	

  
	def RectangleOverlap(r1: Rectangle, r2: Rectangle) = {
	  !{(r2.x + r2.w < r1.x) ||
	  (r2.x > r1.x + r1.w) ||
	  (r2.y + r2.h < r1.y) ||
	  (r2.y > r1.y + r1.h)} 
	}
  
  
  
  
  
  
  
  
}