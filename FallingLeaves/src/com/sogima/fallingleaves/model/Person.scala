package com.sogima.fallingleaves.model

object Person{
	val speed = 5f //units/s

	//States
	object State extends Enumeration{
		type State = Value
		val Walking, Standing = Value
	}
	
	val Width = 60f/480 * MainModel.WIDTH
	val Height = 80f/360 * MainModel.HEIGHT
	
}


class Person(xP: Float, yP: Float){

	import Person._
	import Person.State._

	private var _x = xP
	private var _y = yP
	private var _state = Standing
	private var _facingLeft = true

	def update(delta: Float){	
	  if(state == Walking){
	    x = x + (if(facingLeft){delta * -speed} else{delta * speed})  
	  }
	  else{
	    
	  }
	}
	
	//GETTERS
	def x = _x
	def y = _y
	def rectangle = Rectangle(x, y, Width, Height)
	def state = _state
	def facingLeft = _facingLeft

	//SETTERS
	def state_=(value: State):Unit = _state = value
	private def x_=(value: Float):Unit = _x = value
	private def y_=(value: Float):Unit = _y = value
	def facingLeft_=(value: Boolean):Unit = _facingLeft = value
	
}

