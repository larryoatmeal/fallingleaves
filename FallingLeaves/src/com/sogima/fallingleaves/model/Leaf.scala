package com.sogima.fallingleaves.model

import scala.util.Random

object Leaf{
	val speed = 6f
	val spawnTime = 0.5f
	def newLeaf() = new Leaf(Random.nextFloat() * MainModel.WIDTH, MainModel.HEIGHT)
	var timebomb = 0f
	
	
	val Width = 20f/480 * MainModel.WIDTH
	val Height = 20f/360 * MainModel.HEIGHT
}


class Leaf(xP: Float, yP: Float) {
	import Leaf._
  
	private var _x = xP
	private var _y = yP
	
	def update(delta: Float){
	  y = y - speed * delta
	}
	
	def hitsGround = if(y > 0){false} else {true}
	
	def x = _x
	def y = _y
	def rectangle = Rectangle(x, y, Width, Height)
	
	private def x_=(value: Float):Unit = _x = value
	private def y_=(value: Float):Unit = _y = value
  
}