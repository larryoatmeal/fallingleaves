package com.sogima.fallingleaves.controller

import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.Gdx
import com.sogima.fallingleaves.model.MainModel
import com.sogima.fallingleaves.model.Person


class MainController(model: MainModel) extends InputProcessor{

  //Mutable Map telling us whether each key has been pressed or not

  import scala.collection.mutable.Map
  val keyMap = Map(
    Keys.LEFT -> false,
    Keys.RIGHT -> false
  )


  //This is where we actually update the model based on the current keyMap
  def update(delta: Float){
    if(keyMap(Keys.LEFT)){
      model.person.state = Person.State.Walking
      model.person.facingLeft = true
    }
    else if(keyMap(Keys.RIGHT)){
      model.person.state = Person.State.Walking
      model.person.facingLeft = false
    }
    else{//No key pressed
      model.person.state = Person.State.Standing
    }
  }


  //These methods simply alter the keyMap based on inputs
  override def keyDown(keycode: Int): Boolean = {
    keyMap(keycode) = true
    
    false
  }
  override def keyUp(keycode: Int): Boolean = {
    keyMap(keycode) = false
    
    false
  }
  override def keyTyped(character: Char): Boolean = { 
    false
  }
  override def mouseMoved(screenX: Int,screenY: Int): Boolean = {false} 
  override def scrolled(amount: Int): Boolean = {false} 
  override def touchDown(screenX: Int,screenY: Int,
      pointer: Int,button: Int): Boolean = {
    
    if(screenX > Gdx.graphics.getWidth()/2) keyMap(Keys.RIGHT) = true
    else keyMap(Keys.LEFT) = true
    
    
    false
  } 
  override def touchDragged(screenX: Int,screenY: Int,pointer: Int): Boolean = {false} 
  override def touchUp(screenX: Int,screenY: Int,
      pointer: Int,button: Int): Boolean = {
    if(screenX > Gdx.graphics.getWidth()/2) keyMap(Keys.RIGHT) = false
    else keyMap(Keys.LEFT) = false
    
    false  
  }
  
}