package com.sogima.fallingleaves.screen

import com.badlogic.gdx.Game
import com.badlogic.gdx.Screen
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL10

import com.sogima.fallingleaves.view._
import com.sogima.fallingleaves.controller._
import com.sogima.fallingleaves.model._



class MainScreen(game: Game) extends Screen {
  
	val model = new MainModel()
	val view = new MainView(model)
	val controller = new MainController(model)
	Gdx.input.setInputProcessor(controller)
	
	override def show(){
	  
	}
	override def render(delta: Float){
		Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1)
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT)
		view.render()
		controller.update(delta)
		model.update(delta)
	}
	override def hide(){
	  
	}
	override def pause(){
	  
	}
	override def dispose(){
	  
	}
	override def resize(width: Int, height: Int){
	  
	}
	override def resume(){
	  
	}
}