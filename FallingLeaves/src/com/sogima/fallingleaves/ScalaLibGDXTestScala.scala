//********************************NOT USED******************************************

package com.sogima.fallingleaves;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

class ScalaLibGDXTestScala extends ApplicationListener {
	var camera: OrthographicCamera = _
	var batch: SpriteBatch = _
	var texture: Texture = _
	var sprite: Sprite = _
	
	(1 to 100).map(println(_))
	
	
	override def create() {		
		val w = Gdx.graphics.getWidth();
		val h = Gdx.graphics.getHeight();
		
		camera = new OrthographicCamera(1, h/w)
		batch = new SpriteBatch()
		
		texture = new Texture(Gdx.files.internal("data/libgdx.png"))
		texture.setFilter(TextureFilter.Linear, TextureFilter.Linear)
		
		val region = new TextureRegion(texture, 0, 0, 512, 275)
		
		sprite = new Sprite(region)
		sprite.setSize(0.9f, 0.9f * sprite.getHeight() / sprite.getWidth())
		sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2)
		sprite.setPosition(-sprite.getWidth()/2, -sprite.getHeight()/2)
		
	
	}


	override def dispose() {
		batch.dispose()
		texture.dispose()
	}

	override def render() {		
		Gdx.gl.glClearColor(0, 1, 1, 1)
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT)
		
		batch.setProjectionMatrix(camera.combined)
		batch.begin()
		sprite.draw(batch)
		batch.end()
	}

	override def resize(width: Int, height:Int) {
	}

	
	override def pause() {
	}

	override def resume() {
	}
}
