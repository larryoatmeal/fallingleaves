package com.sogima.fallingleaves

object Worksheet {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  

	object Person{
		val velocity = 1
		val width = 1
		val height = 1
	
		//States
		object State extends Enumeration{
			type State = Value
			val Walking, Standing = Value
		}
	
	}


	class Person(xP: Int, yP: Int){
	
		import Person._
		import Person.State._
	
		private var _x = xP
		private var _y = yP
		private var _state = Standing
	
	
	
		def update(delta: Float){
	
			
		}
		
		//GETTERS
		def x = _x
		def y = _y
		def state = _state
	
		//SETTERS
		def state_=(stateP: State):Unit = _state = stateP
		def x_=(value: Int):Unit = _x = value
	}

  val p = new Person(10, 20)                      //> p  : com.sogima.fallingleaves.Worksheet.Person = com.sogima.fallingleaves.Wo
                                                  //| rksheet$$anonfun$main$1$Person$3@5f8aa07
  
	p.x                                       //> res0: Int = 10
  p.y                                             //> res1: Int = 20
  p.state                                         //> res2: com.sogima.fallingleaves.Worksheet.Person.State.Value = Standing
  p.state = Person.State.Walking
  p.state                                         //> res3: com.sogima.fallingleaves.Worksheet.Person.State.Value = Walking
}