package com.sogima.fallingleaves

object Worksheet {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(96); 
  println("Welcome to the Scala worksheet")
  
  

	object Person{
		val velocity = 1
		val width = 1
		val height = 1
	
		//States
		object State extends Enumeration{
			type State = Value
			val Walking, Standing = Value
		}
	
	}


	class Person(xP: Int, yP: Int){
	
		import Person._
		import Person.State._
	
		private var _x = xP
		private var _y = yP
		private var _state = Standing
	
	
	
		def update(delta: Float){
	
			
		}
		
		//GETTERS
		def x = _x
		def y = _y
		def state = _state
	
		//SETTERS
		def state_=(stateP: State):Unit = _state = stateP
		def x_=(value: Int):Unit = _x = value
	};$skip(590); 

  val p = new Person(10, 20);System.out.println("""p  : com.sogima.fallingleaves.Worksheet.Person = """ + $show(p ));$skip(8); val res$0 = 
  
	p.x;System.out.println("""res0: Int = """ + $show(res$0));$skip(6); val res$1 = 
  p.y;System.out.println("""res1: Int = """ + $show(res$1));$skip(10); val res$2 = 
  p.state;System.out.println("""res2: com.sogima.fallingleaves.Worksheet.Person.State.Value = """ + $show(res$2));$skip(33); 
  p.state = Person.State.Walking;$skip(10); val res$3 = 
  p.state;System.out.println("""res3: com.sogima.fallingleaves.Worksheet.Person.State.Value = """ + $show(res$3))}
}
